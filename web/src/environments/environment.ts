export const environment = {
    production: false,
    // endPoint: 'https://ocrm.capitaland.com.cn/api_proxy',
    // endPoint: 'http://localhost:8087',
    // endPoint: 'https://ocrm.capitaland.com.cn/uat',
    endPoint: 'http://ocrm.capitaland.com.cn:8088',
    baseHref: '/'
};
