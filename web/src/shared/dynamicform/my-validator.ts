import { FormGroup, ValidatorFn, AbstractControl } from '@angular/forms';

// 提取信息中的网络链接:(h|H)(r|R)(e|E)(f|F) *= *('|")?(\w|\\|\/|\.)+('|"| *|>)?
// 提取信息中的邮件地址:\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*
// 提取信息中的图片链接:(s|S)(r|R)(c|C) *= *('|")?(\w|\\|\/|\.)+('|"| *|>)?
// 提取信息中的IP地址:(\d+)\.(\d+)\.(\d+)\.(\d+)
// 提取信息中的中国电话号码（包括移动和固定电话）:(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}
// 提取信息中的中国邮政编码:[1-9]{1}(\d+){5}
// 提取信息中的中国身份证号码:\d{18}|\d{15}
// 提取信息中的整数：\d+
// 提取信息中的浮点数（即小数）：(-?\d*)\.?\d+
// 提取信息中的任何数字 ：(-?\d*)(\.\d+)?
// 提取信息中的中文字符串：[\u4e00-\u9fa5]*
// 提取信息中的双字节字符串 (汉字)：[^\x00-\xff]*

export function EmailValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        if (!control.value || control.value !== '') {
            return null;
        }
        const email = control.value;
        // const no = /\w@\w*\.\w/.test(email);
        const no = /\w@([A-Za-zd]+[-.])+[A-Za-zd]{2,5}$/.test(email);
        return no ? null : { Email: { email } };
    };
}

export function MobileValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const mobile = control.value;
        const no = /^(((1[0-9][0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(
            mobile
        );
        return no ? null : { Mobile: { mobile } };
    };
}

export function PhoneValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const phone = control.value;
        const no = /^(\(\d{3,4}\)|\d{3,4}-|\s)?\d{7,14}$/.test(phone);
        const no2 = /^(((1[0-9][0-9]{1})|(15[0-9]{1})|(18[0-9]{1}))+\d{8})$/.test(
            phone
        );
        return no || no2 ? null : { Phone: { phone } };
    };
}

export function NumberValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const num = control.value;
        const no = /^[0-9]*$/.test(num);
        return no ? null : { Number: { num } };
    };
}

export function PasswordValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const password = control.value;
        let no = true;
        if (password == null || password.length < 6) {
            no = false;
        }
        let reg1 = new RegExp(/^[0-9A-Za-z]+$/);
        if (!reg1.test(password)) {
            no = false;
        }
        var reg = new RegExp(/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/);
        if (!reg.test(password)) {
            no = false;
        }
        return no ? null : { Password: { password } };
    };
}
// export function EmailValidator(nameRe: RegExp): ValidatorFn {
//     return (control: AbstractControl): { [key: string]: any } => {
//         const name = control.value;
//         const no = nameRe.test(name);
//         return no ? { 'forbiddenName': { name } } : null;
//     };
// }
