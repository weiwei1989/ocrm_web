import { ControlBase } from './controlbase';

export class SelectBox extends ControlBase<string> {

    controlType = 'selectbox';

    options: { key: string, value: string }[] = [];


    constructor(options: {} = {}) {
        super(options);
        this.options = options['options'] || [];
    }
}