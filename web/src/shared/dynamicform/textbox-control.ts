import { ControlBase } from './controlbase';

export class TextBox extends ControlBase<string> {

    controlType = 'textbox';

    type: string;

    placeHolder: string;

    constructor(options: {} = {}) {
        super(options);
        this.type = options['type'] || '';
        this.placeHolder = options['placeHolder'] || '';
    }
}