import { ControlBase } from './controlbase';

export class DateTimeBox extends ControlBase<string> {

    controlType = 'datetime';

    displayFormat: string;

    pickerFormat: string;

    constructor(options: {} = {}) {
        super(options);
        this.displayFormat = options['format'] || 'YYYY-MM-DD';
        this.pickerFormat = options['pickerFormat'] || 'DDDD MMM D, YYYY';
    }
}