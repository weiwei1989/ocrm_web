import { FormControl, FormGroup, Validators } from '@angular/forms';

export class ControlBase<T>{
    value: T;
    key: string;
    label: string;
    required: boolean;
    width: number;
    order: number;
    controlType: string;
    groupId: number;
    validators: Validators;
    disabled: boolean;

    constructor(options: {
        value?: T,
        key?: string,
        label?: string,
        required?: boolean,
        width?: number,
        order?: number,
        controlType?: string,
        groupId?: number,
        validators?: Validators,
        disabled?: boolean
    } = {}) {
        this.value = options.value;
        this.key = options.key || '';
        this.label = options.label || '';
        this.required = !!options.required;
        this.order = typeof options.order === undefined ? 1 : options.order;
        this.controlType = options.controlType || '';
        this.width = options.width || 100;
        this.groupId = options.groupId;
        this.validators = typeof options.validators === undefined ? [] : options.validators;
        this.disabled = typeof options.disabled === 'undefined' ? false : options.disabled;
    }
}
