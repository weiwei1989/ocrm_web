import { ControlBase } from './controlbase';

export class TextArea extends ControlBase<string> {

    controlType = 'textarea';

    type: string;

    constructor(options: {} = {}) {
        super(options);
        this.type = options['type'] || '';
    }
}