import { TextBox } from './textbox-control';
import { ControlBase } from './controlbase';
import { SelectBox } from './selectbox-control';
import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import { MdlButtonComponent, MdlCheckboxComponent, MdlLayoutComponent, MdlTextFieldComponent } from '@angular-mdl/core';
// import { MdlSelectModule } from '@angular-mdl/select'

@Component({
    selector: 'df-control',
    templateUrl: 'control.component.html'
})
export class ControlComponent implements OnInit {
    @Input() ctrlbox: ControlBase<any>;
    @Input() form: FormGroup;
    get isValid() {
        if (!this.form.controls[this.ctrlbox.key]) {
            return false;
        }
        else {
            return this.form.controls[this.ctrlbox.key].valid
                || this.form.controls[this.ctrlbox.key].pristine;
        }
    }

    get errors() {
        let errStr = this.ctrlbox.label;
        let errs = this.form.get(this.ctrlbox.key).errors;
        for (const key in errs) {
            errStr += `${key !== 'required' ? '格式非法' : '必填项'}`;
            console.log(`%c-----${key}-----`, 'color: red');
        }
        return errStr;
        // return this.form.controls[this.ctrlbox.key].errors[]
    }
    constructor() { }

    ngOnInit() {
        if (this.ctrlbox.controlType === 'selectbox') {
            // let sb = <SelectBox>this.ctrlbox;
            // sb.options = [];
        }
    }

    ngOnChanges(...args: any[]) {
        console.log('%c........onChange fired', 'color: yellow;');
        console.log('changing', args);
        if (!args[0].ctrlbox.currentValue) {
            return;
        }
    }
}
