import { FloorService } from '../../services/floor-service';
import { CompanyService } from '../../services/company-service';
import { ProjectService } from '../../services/project-service';
import { ManageService } from '../../services/manage-service';
import { BuildingService } from '../../services/building-service';
import { SessionService } from '../../services/session-service';
import { UserService } from '../../services/user-service';
import { LocationSelectComponent } from './location-select.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MdlModule } from "@angular-mdl/core/components";
import { FormsModule } from "@angular/forms";
import { MdlSelectModule } from "@angular-mdl/select";

@NgModule({
  imports: [
    CommonModule,
    MdlModule,
    MdlSelectModule,
    FormsModule
  ],
  declarations: [LocationSelectComponent],
  exports: [LocationSelectComponent],
  providers: [
    FloorService,
    CompanyService,
    ProjectService,
    ManageService,
    BuildingService,
    SessionService,
    UserService,
    SessionService
  ]
})
export class LocationSelectModule { }
