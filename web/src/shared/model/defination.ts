export interface Identity {
    id?: number;
}
export interface Audit extends Identity {
    created_at?: Date;
    updated_at?: Date;
    deleted_at?: Date;
}
export interface Company extends Audit {
    id?: number;
    name: string;
    phone: string;
    locations: Array<Location>;
}
export interface Admin extends Audit {
    avatar?: string;
    companyId: number;
    email?: string;
    gender?: number;
    idCard?: string;
    userName?: string;
    mobile?: string;
    password?: string;
    createdBy?: number;
    status?: number;
    company: Company;
    roles: Array<Role>;
    managements?: any;
}
export interface Manage extends Audit {
    avatar?: string;
    companyId: number;
    email?: string;
    gender?: number;
    idCard?: string;
    userName?: string;
    mobile?: string;
    password?: string;
    createdBy?: number;
    status?: number;
    company: Company;
    roles: Array<Role>;
}

export interface Project extends Audit {
    id?: number;
    name: string;
    locationCode: string;
    createdBy?: number;
    provinceCode?: string;
    cityCode?: string;
    faceApiUserId?: string;
    faceApiPass?: string;
    district?: string;
    buildings?: Array<Building>;
}

export interface Building extends Audit {
    id?: number;
    buildingNumber?: string;
    buildingAlias?: string;
    createdBy?: number;
    projectId?: number;
    project?: Project;
    floors?: Array<Floor>;
}

export interface Floor extends Audit {
    id?: number;
    buildingId?: number;
    createdBy?: number;
    index?: string;
    alias?: string;
    building?: Building;
    units?: Array<Unit>;
}

export interface Unit extends Audit {
    name?: string;
    floorId?: number;
    Floor?: Floor;
}

export interface Location extends Audit {
    project: Project;
    building: Building;
    floor: Floor;
    unit: Unit;
}

export interface Photo extends Audit {
    path: string;
    facePhotoId: number;
    facePhotoPath: string;
    createdBy: number;
}

export interface Role extends Audit {
    displayOrder: number;
    roleName: string;
}
export interface User extends Audit {
    avatar?: string;
    companyId: number;
    email?: string;
    gender?: number;
    idCard?: string;
    userName?: string;
    mobile?: string;
    password?: string;
    createdBy?: number;
    status?: number;
    Company: Company;
    Photos: Array<Photo>;
}

export interface SearchRet<T> {
    pageNumber: number;
    totalPages: number;
    totalPageRecords: number;
    items: [T];
}
