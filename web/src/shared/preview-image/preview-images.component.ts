import { Component, OnInit, ViewChild, HostListener, Inject, InjectionToken } from '@angular/core';
import { MdlDialogReference } from "@angular-mdl/core";

export const PHOTO_URL = new InjectionToken<string>('');

@Component({
  selector: 'preview-image',
  templateUrl: 'preview-images.component.html',
  styleUrls: ['preview-images.component.scss']
})
export class PreviewImagesComponent {

  photoPath: string = '';

  constructor(
    private dialog: MdlDialogReference,
    @Inject(PHOTO_URL) photoUrl: string
  ) {
    this.photoPath = photoUrl;
    // register a listener if you want to be informed if the dialog is closed.
    this.dialog.onHide().subscribe((user) => {
      console.log('login dialog hidden');
      if (user) {
        console.log('authenticated user', user);
      }
    });
  }

  public close() {
    console.log('login', this.dialog);
    this.dialog.hide();
  }

  @HostListener('keydown.esc')
  public onEsc(): void {
    this.dialog.hide();
  }
}