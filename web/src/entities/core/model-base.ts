/**
 * SortType
 */
export enum SortType {
    Ascending = 0,
    Descending = 1,
}

export class IdentifiableEntity {
    id: number;
    isDeleted: boolean;

    constructor(data?: any) {
        if (data !== undefined) {
            this.id = data["Id"] !== undefined ? data["Id"] : null;
            this.id = data["isDeleted"] !== undefined ? data["isDeleted"] : null;
        }
    }

    static fromJS(data: any): IdentifiableEntity {
        return new IdentifiableEntity(data);
    }

    toJS(data?: any) {
        data = data === undefined ? {} : data;
        data["Id"] = this.id !== undefined ? this.id : null;
        data["isDeleted"] = this.id !== undefined ? this.id : null;
        return data;
    }

    toJSON() {
        return JSON.stringify(this.toJS());
    }

    clone() {
        var json = this.toJSON();
        return new IdentifiableEntity(JSON.parse(json));
    }
}

/**
 * base model with Id
 */
export class AuditableEntity {
    createdBy: number;
    createdDate: Date;
    lastModifiedBy: number;
    lastModifiedDate: Date;
    id: number;

    constructor(data?: any) {
        if (data !== undefined) {
            this.createdBy = data["CreatedBy"] !== undefined ? data["CreatedBy"] : null;
            this.createdDate = data["CreatedDate"] ? new Date(data["CreatedDate"].toString()) : null;
            this.lastModifiedBy = data["LastModifiedBy"] !== undefined ? data["LastModifiedBy"] : null;
            this.lastModifiedDate = data["LastModifiedDate"] ? new Date(data["LastModifiedDate"].toString()) : null;
            this.id = data["Id"] !== undefined ? data["Id"] : null;
        }
    }

    static fromJS(data: any): AuditableEntity {
        return new AuditableEntity(data);
    }

    toJS(data?: any) {
        data = data === undefined ? {} : data;
        data["CreatedBy"] = this.createdBy !== undefined ? this.createdBy : null;
        data["CreatedDate"] = this.createdDate ? this.createdDate.toISOString() : null;
        data["LastModifiedBy"] = this.lastModifiedBy !== undefined ? this.lastModifiedBy : null;
        data["LastModifiedDate"] = this.lastModifiedDate ? this.lastModifiedDate.toISOString() : null;
        data["Id"] = this.id !== undefined ? this.id : null;
        return data;
    }

    toJSON() {
        return JSON.stringify(this.toJS());
    }

    clone() {
        var json = this.toJSON();
        return new AuditableEntity(JSON.parse(json));
    }
}
