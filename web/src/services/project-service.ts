import { Project, SearchRet } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { ProjectSearchCriteria, SortType } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ProjectService extends BaseGenericService<
    Project,
    ProjectSearchCriteria
> {
    constructor(http: HttpClient, storage: LocalStorageService) {
        super(http, 'project', storage);
    }

    public getProjectsWithBuildings(): Observable<SearchRet<Project>> {
        return this.Search(null);
    }

    public getSummary(projectId: number): Observable<any> {
        return this.http.get(`${this.api}/project_summary/${projectId}`);
    }

    // public getB2CUser(): Observable<User[]> {
    //     let criteria = <UserSearchCriteria>{
    //     };
    //     return <Observable<User[]>>this.Search(criteria)
    // }
}
