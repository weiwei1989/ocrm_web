import { Injectable } from '@angular/core';
import { BaseGenericService } from './core/base-generic-service';
import { HttpClient } from '@angular/common/http';
import { LocalStorageService } from 'angular-2-local-storage';
import { Observable } from 'rxjs/Observable';
import { SearchRet } from '../shared/model/defination';
import { DOMAIN, API_VERSION } from '../app.config';

@Injectable()
export class VisitService extends BaseGenericService<any, any> {
    constructor(http: HttpClient, storage: LocalStorageService) {
        super(http, 'visit_request', storage);
    }

    public approve(ids: any[], projectId: any): Observable<any[]> {
        return <Observable<any[]>>this.http.post(
            `${DOMAIN}/api/${API_VERSION}/approve_requests`,
            {
                ids: ids,
                project_id: projectId
            }
        );
    }
    // public getB2CUser(): Observable<User[]> {
    //     let criteria = <UserSearchCriteria>{
    //     };
    //     return <Observable<User[]>>this.Search(criteria)
    // }
}
