import { Company, SearchRet } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { CompanySearchCriteria, SortType } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CompanyService extends BaseGenericService<Company, any> {
    constructor(http: HttpClient, storage: LocalStorageService) {
        super(http, 'company', storage);
    }
    public getCompanyiesByFloorId(
        floorId: number
    ): Observable<SearchRet<Company>> {
        const criteria: CompanySearchCriteria = new CompanySearchCriteria({
            IsDeleted: false,
            FloorId: floorId,
            PageNumber: 1,
            PageSize: 10000,
            SortBy: 'Id',
            SortType: SortType[SortType.Descending]
        }).toJS();
        return <Observable<SearchRet<Company>>>this.Search(criteria);
    }

    public getCompanybyUnitId(unitId: number): Observable<SearchRet<Company>> {
        const criteria: any = {
            unitId: unitId
        };
        return <Observable<SearchRet<Company>>>this.Search(criteria);
    }

    getAllCompanyOfProject(projectId: number): Observable<SearchRet<Company>> {
        const criteria: any = {
            projectId: projectId,
            pageSize: 1000
        };
        return <Observable<SearchRet<Company>>>this.Search(criteria);
    }

    getCompanyAll(projectId: number): Observable<any> {
        return this.http.post(`${this.api}/raw_search`, {
            projectId: projectId
        });
    }
    // public getAllBuildings(): Observable<Building[]> {
    // 	let criteria: BuildingSearchCriteria = new BuildingSearchCriteria({
    // 		pageNumber: 1,
    // 		pageSize: 10000,
    // 		sortBy: 'Id',
    // 		sortType: SortType[SortType.Descending]
    // 	}).toJS();
    // 	return <Observable<Building[]>>this.Search(criteria);
    // }
}
