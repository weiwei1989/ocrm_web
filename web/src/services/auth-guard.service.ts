import { SessionService } from './session-service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate, CanActivateChild {
	constructor(
		private _sessionService: SessionService,
		private _router: Router
	) { }

	canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
		let url: string = state.url;

		return this.checkLogin('');
	}

	canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
		return this.canActivate(route, state);
	}

	checkLogin(url: string): boolean {
		if (this._sessionService.isAuth()) { return true; }

		// Store the attempted URL for redirecting
		// this.authService.redirectUrl = url;

		// Navigate to the login page with extras
		this._router.navigate(['/login']);

		return false;
	}
}
