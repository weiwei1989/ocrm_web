import { Building, SearchRet } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { BuildingSearchCriteria, SortType } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class BuildingService extends BaseGenericService<Building, BuildingSearchCriteria>{

	constructor(http: HttpClient, storage: LocalStorageService) {
		super(http, 'building', storage);
	}

	public getAllBuildings(): Observable<SearchRet<Building>> {
		let criteria: BuildingSearchCriteria = BuildingSearchCriteria.fromJS({
			IsDeleted: false,
			PageNumber: 1,
			PageSize: 10000,
			SortBy: 'Id',
			SortType: SortType[SortType.Descending]
		}).toJS();
		console.log(criteria)
		// .toJS();
		return <Observable<SearchRet<Building>>>this.Search(criteria);
	}
}