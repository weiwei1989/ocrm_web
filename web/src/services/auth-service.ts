import { Admin } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { User } from '../entities/entities';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
// Statics
import 'rxjs/add/observable/throw';

// Operators
import 'rxjs/add/operator/catch';

import { BaseService } from './core/base-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { SessionService } from './session-service';
import { HttpClient, HttpResponse } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";



@Injectable()
export class AuthService extends BaseService {

    constructor(
        private http: HttpClient,
        private _localstorageService: LocalStorageService,
        private _sesseionService: SessionService
    ) {
        super('token');
    }

    public test(): Observable<any> {
        return this.http.get('http://localhost:5000/api/values/1');
    }

    public login(credentail: { email: string, password: string }) {
        let body = { email: credentail.email, password: credentail.password };
        return this.http.post(this.api, body);

        // .map((res: Response) => {
        //     let body = res.json();
        //     this._sesseionService.create(body.token);
        //     return res.status === 200;
        // })
        // .catch((error: any) => {
        //     let errMsg = (error.message) ? error.message :
        //         error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        //     console.error(errMsg); // log to console instead
        //     return Observable.throw(errMsg);
        // });

    }
}