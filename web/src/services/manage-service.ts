import { Admin, Manage } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RequestMethod, Http } from '@angular/http';
import { User, UserSearchCriteria } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from "@angular/common/http";
import { DOMAIN, API_VERSION } from "app.config";

@Injectable()
export class ManageService extends BaseGenericService<Manage, UserSearchCriteria>{

    constructor(http: HttpClient, storage: LocalStorageService) {
        super(http, 'admin', storage);
    }

    public changePassword(userId: number, oldPass: string, newPass: string): Observable<any> {
        return this.http.post(`${this.api}/change_pass/${userId}`, {
            oldPass: oldPass,
            newPass: newPass
        });
    }

    public async batchUpdate(data: Array<any>): Promise<Array<Manage>> {
        let url = `${DOMAIN}/api/${API_VERSION}/manage/batchUpdate`;
        return await this.http.put<Array<Manage>>(url, data).toPromise();
    }
}