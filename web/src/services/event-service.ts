import { Floor, SearchRet } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { FloorSearchCriteria, SortType } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from '@angular/common/http';
import { URLSearchParams } from '@angular/http/src/url_search_params';
import { Object } from 'core-js/library/web/timers';
import { HttpParams } from '@angular/common/http/src/params';

@Injectable()
export class EventService extends BaseGenericService<any, any> {
    constructor(http: HttpClient, storage: LocalStorageService) {
        super(http, 'events', storage);
    }

    private _serialize(obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(
                    encodeURIComponent(p) + '=' + encodeURIComponent(obj[p])
                );
            }
        return str.join('&');
    }

    public async searchEvents(criteria?: any | object): Promise<any> {
        const queryParams = this._serialize(criteria);
        return await this.http
            .get<any>(`${this.api}?${queryParams}`)
            .toPromise();
    }
}
