import { Injectable } from '@angular/core';
import { API_VERSION, DOMAIN } from '../../app.config';


/**
 * base http config ,means source
 */
@Injectable()
export abstract class BaseService {

    protected api: string;

    private excludeApis: string[] = [
        'Authorizations'
    ];

    constructor(apiParam: string) {
        this.api = this.excludeApis.indexOf(apiParam) === -1
            ? `${DOMAIN}/api/${API_VERSION}/${apiParam}`
            : `${DOMAIN}/${API_VERSION}/${apiParam}`;
        console.log('this.api.', this.api);
    }

}