import { API_VERSION } from '../app.config';
import { FloorService } from './floor-service';
import { BuildingService } from './building-service';
import { ProjectService } from './project-service';
import {
  Building,
  Floor,
  Location,
  Project,
  Unit
} from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { LocationSearchCriteria } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from '@angular/common/http';
import { DOMAIN } from 'app.config';

interface LocationConfig {
  projects: Array<Project>;
  buildings: Array<Building>;
  floors: Array<Floor>;
  units: Array<Unit>;
}

@Injectable()
export class LocationService extends BaseGenericService<Location, any> {
  constructor(
    storage: LocalStorageService,
    private _http: HttpClient,
    private _storage: LocalStorageService,
    private _projectService: ProjectService,
    private _buildingService: BuildingService,
    private _floorService: FloorService
  ) {
    super(_http, 'location', storage);
  }

  public getAllBuildingsByProjectId(
    projectId: number
  ): Observable<Array<Building>> {
    return this._buildingService.All({ projectId: projectId });
  }

  public getAllFloorsByBuildingId(
    buildingId: number
  ): Observable<Array<Floor>> {
    return this._floorService.All({ buildingId: buildingId });
  }

  public getAllUnitsByFloorId(floorId: number): Observable<Array<Unit>> {
    return this._http.post<Array<Unit>>(
      `${DOMAIN}/api/${API_VERSION}/unit/all`,
      { floorId: floorId }
    );
  }
}
