import { Admin } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RequestMethod, Http } from '@angular/http';
import { User, UserSearchCriteria } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class UserService extends BaseGenericService<Admin, UserSearchCriteria>{

    constructor(http: HttpClient, storage: LocalStorageService) {
        super(http, 'staff', storage);
    }

    public changePassword(userId: number, oldPass: string, newPass: string): Observable<any> {
        return this.http.post(`${this.api}/change_pass/${userId}`, {
            oldPass: oldPass,
            newPass: newPass
        });
        // return this._httpAuthWrapper(RequestMethod.Post, `${this.api}/change_pass/${userId}`, {
        //     oldPass: oldPass,
        //     newPass: newPass
        // });
    }
}