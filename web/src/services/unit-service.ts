import { Floor, SearchRet, Unit } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { FloorSearchCriteria, SortType } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class UnitService extends BaseGenericService<Unit, any>{

    constructor(http: HttpClient, storage: LocalStorageService) {
        super(http, 'unit', storage);
    }


}