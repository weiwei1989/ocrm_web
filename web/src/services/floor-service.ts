import { Floor, SearchRet } from '../shared/model/defination';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http } from '@angular/http';
import { FloorSearchCriteria, SortType } from '../entities/entities';
import { BaseGenericService } from './core/base-generic-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { HttpClient } from "@angular/common/http";

@Injectable()
export class FloorService extends BaseGenericService<Floor, FloorSearchCriteria>{

	constructor(http: HttpClient, storage: LocalStorageService) {
		super(http, 'floor', storage);
	}

	public getAllFloors(): Observable<SearchRet<Floor>> {
		let criteria: FloorSearchCriteria = new FloorSearchCriteria({
			// IsDeleted: false,
			// pageNumber: 1,
			// pageSize: 10000,
			// sortBy: 'Id',
			// sortType: SortType[SortType.Descending]
		}).toJS();
		return <Observable<SearchRet<Floor>>>this.Search(criteria);
	}
}