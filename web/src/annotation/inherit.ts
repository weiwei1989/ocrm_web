import { Component, Injectable } from '@angular/core';
import 'reflect-metadata';

export function Inherit(annotation: any) {
	return function(target: Function) {
		var parentTarget = Object.getPrototypeOf(target.prototype).constructor;
		var parentAnnotations = Reflect.getMetadata('design:paramtypes', parentTarget);

		Reflect.defineMetadata('design:paramtypes', parentAnnotations, target);
	}
}