import { AuthGuard } from '../services/auth-guard.service';
import { RegisterComponent } from './register/register.component';
import { ManageEditComponent } from './manage-edit/manage-edit.component';
import { ManageComponent } from './manage/manage.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProjectComponent } from './project/project.component';
import { CompanyComponent } from './company/company.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { ProjectEditComponent } from './project/project-edit/project-edit.component';
import { ProjectIndexComponent } from './project/project-index/project-index.component';
import { FloorComponent } from './floor/floor.component';
import { IndexComponent as FloorIndexComponent } from './floor/index/index.component';
import { EditComponent as FloorEditComponent } from './floor/edit/edit.component';
import { IndexComponent as CompanyIndexComponent } from './company/index/index.component';
import { EditComponent as CompanyEditComponent } from './company/edit/edit.component';
import { IndexComponent as UserIndexComponent } from './user/index/index.component';
import { EditComponent as UserEditComponent } from './user/edit/edit.component';
import { ChangepassComponent } from './user/changepass/changepass.component';
import { EventComponentComponent } from 'app/event-component/event-component.component';
import { DashboardComponent } from 'app/dashboard/dashboard.component';
import { VistCmpComponent } from './vist-cmp/vist-cmp.component';
// location === building

const routes: Routes = [
  { path: '', redirectTo: '/project', pathMatch: 'full' },
  {
    path: 'change_pass',
    canActivate: [AuthGuard],
    component: ChangepassComponent
  },
  { path: 'login', component: LoginComponent },
  { path: 'manage', canActivate: [AuthGuard], component: ManageComponent },
  { path: 'events', component: EventComponentComponent },
  {
    path: 'manage-edit/:id',
    canActivate: [AuthGuard],
    component: ManageEditComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'manage-edit',
    canActivate: [AuthGuard],
    component: ManageEditComponent
  },
  { path: 'register', component: RegisterComponent },
  {
    path: 'project',
    component: ProjectComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        canActivateChild: [AuthGuard],
        children: [
          { path: '', component: ProjectIndexComponent },
          { path: 'edit/:id', component: ProjectEditComponent },
          { path: 'edit', component: ProjectEditComponent }
        ]
      }
    ]
  },
  {
    path: 'floor',
    component: FloorComponent,
    children: [
      { path: '', component: FloorIndexComponent },
      { path: 'edit/:id', component: FloorEditComponent },
      { path: 'edit', component: FloorEditComponent }
    ]
  },
  {
    path: 'company',
    component: CompanyComponent,
    children: [
      { path: '', component: CompanyIndexComponent },
      { path: 'edit/:id', component: CompanyEditComponent },
      { path: 'edit', component: CompanyEditComponent }
    ]
  },
  {
    path: 'user',
    component: UserComponent,
    children: [
      { path: '', component: UserIndexComponent },
      { path: 'edit/:id', component: UserEditComponent },
      { path: 'edit', component: UserEditComponent }
    ]
  },
  {
    path: 'visit',
    component: VistCmpComponent,
    children: [
      {
        path: '',
        component: VistCmpComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
