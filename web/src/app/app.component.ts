import { Component, ViewChild, OnInit } from '@angular/core';
import {
    MdlButtonComponent,
    MdlCheckboxComponent,
    MdlLayoutComponent
} from '@angular-mdl/core';
import { Menu, slideMenus } from 'app.config';
import { Router } from '@angular/router';
import { SessionService } from '../services/session-service';
import { Admin } from 'shared/model/defination';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [SessionService]
})
export class AppComponent implements OnInit {
    roleToRedirect: any[] = [
        { name: '/project', title: '项目管理' },
        { name: '/floor', title: '楼层管理' },
        { name: '/user', title: '用户管理' },
        { name: '/location', title: '大楼管理' },
        { name: '/company', title: '公司管理' },
        { name: '/property', title: '建筑物/楼层/单元号管理' },
        { name: '/manage', title: '管理员列表' },
        { name: '/visit', title: '访客列表' }
    ];

    public isLoading = false;

    @ViewChild('appLayout') _appLayout: MdlLayoutComponent;

    public currentMenu: Menu = slideMenus[0];

    public slideMenus: Menu[] = slideMenus;

    public IsLogin = true;

    private _sub: any;

    public userRole: string;

    public sessionUser: Admin;

    public pageTitle: string;

    public managable: any = this._sessionService.Managable;

    public titleMap: any = {
        project: '项目管理',
        floor: '楼层单元管理',
        user: '用户管理',
        // 'location': '大楼管理',
        company: '公司管理'
    };

    constructor(
        private _router: Router,
        private _sessionService: SessionService
    ) {}

    async ngOnInit() {
        try {
            if (this._sessionService.isUserProperty) {
                const index = this.slideMenus.findIndex(
                    s => s.path === '/manage'
                );
                if (index !== -1) {
                    this.slideMenus[index].text = '公司行政列表';
                }
            }
            this.sessionUser = this._sessionService.getCurrentUser();
            let userRoles = this._sessionService.getRoles();
            this.userRole = userRoles && userRoles.length ? userRoles[0] : '';
            this._sub = this._router.events.subscribe((event: any) => {
                console.log('............');
                console.log(event);
                this.IsLogin = event.url && event.url.indexOf('login') !== -1;
                // this.IsLogin && (this.userRole = null);
                const currentUrl = event.url;
                if (!currentUrl) {
                    return;
                }
                console.log(currentUrl);

                const titleObj = this.roleToRedirect.find(
                    rd => rd.name === currentUrl
                );
                this.pageTitle = titleObj ? titleObj.title : '';
                // if (currentUrl.indexOf('project') !== -1)
                //     this.pageTitle = '项目管理';
                // if (currentUrl.indexOf('floor') !== -1)
                //     this.pageTitle = '楼层管理';
                // if (currentUrl.indexOf('user') !== -1)
                //     this.pageTitle = '用户管理';
                // if (currentUrl.indexOf('location') !== -1)
                //     this.pageTitle = '大楼管理';
                // if (currentUrl.indexOf('company') !== -1)
                //     this.pageTitle = '公司管理';
                // if (currentUrl.indexOf('property') !== -1)
                //     this.pageTitle = '建筑物/楼层/单元号管理';
                // if (currentUrl.indexOf('manage') !== -1)
                //     this.pageTitle = '管理员列表';
            });
        } catch (e) {
            console.error(e);
            if (e.status === 401) {
                this._router.navigate(['/login']);
            }
        } finally {
        }
    }
    ngOnDestroy() {
        this._sub.unsubscribe();
    }
    // ngAfterViewChecked() {
    // 	console.log('ngAfterViewChecked');
    // 	console.log(this.isLoading);
    // 	setTimeout(() => { this.isLoading = false }, 1000);
    // }
    changePass() {
        this._router.navigate(['/change_pass']);
    }

    public canAccess(roles: Array<string>): boolean {
        return roles.indexOf(this.userRole) !== -1;
    }

    public logout(): void {
        this._sessionService.destroy();
        this._router.navigate(['/login']);
    }

    public navLinkClick(index): void {
        this.currentMenu = slideMenus[index];
        this._appLayout.closeDrawer();
    }
}
