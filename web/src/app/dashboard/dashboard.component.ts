import { Component, OnInit } from '@angular/core';
import { ProjectService } from 'services/project-service';
import { SessionService } from 'services/session-service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
    status: any = {};

    constructor(
        private _projectService: ProjectService,
        private _sessionService: SessionService
    ) {}

    async ngOnInit() {
        const manageId = this._sessionService.Managable.id;
        const ret = await this._projectService.getSummary(manageId).toPromise();
        console.log(ret);
        const [userWithPhotoCount, userCount] = ret;
        this.status.userWithPhotoCount =
            userWithPhotoCount.USER_WITH_PHOTO_COUNT;
        this.status.userCount = userCount.USER_COUNT;
    }
}
