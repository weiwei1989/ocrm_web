import { Project } from '../../../shared/model/defination';
import { Component, OnInit } from '@angular/core';
import { Helper } from '../../common/index';
import { ControlBase } from '../../../shared/dynamicform/controlbase';
import { TextBox } from '../../../shared/dynamicform/textbox-control';
import { EmailValidator } from '../../../shared/dynamicform/my-validator';
import { FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs/Rx';
import { Router, ActivatedRoute, Params, Route } from '@angular/router';
import { ProjectService } from '../../../services/project-service';
import { ProjectRequest } from '../../../dtos/projectRequest';
// import { Project } from '../../../entities/entities';
import { MdlSnackbarService } from '@angular-mdl/core';

@Component({
	selector: 'app-project-edit',
	templateUrl: './project-edit.component.html',
	styleUrls: ['./project-edit.component.scss'],
	providers: [ProjectService, MdlSnackbarService]
})
export class ProjectEditComponent implements OnInit {

	public isPosting: boolean = false;

	public get IsEdit() {
		return this._route.snapshot.params['id'] !== undefined;
	}

	private _editProject: Project;

	public isFormPosting: boolean = false;

	formInputs: ControlBase<any>[];

	formControls: any;

	constructor(
		private _route: ActivatedRoute,
		private _router: Router,
		private _projectService: ProjectService,
		private _mdlSnackbarService: MdlSnackbarService
	) { }

	async ngOnInit() {
		try {
			this.isPosting = true;
			let projectId = this.IsEdit ? +this._route.snapshot.params['id'] : 0;
			if (projectId) {
				this._editProject = await this._projectService.Get(projectId).toPromise();
			}
			this.formControls = this._setForm(this._editProject);

		}
		catch (e) {
			if (e.status === 401) {
				this._router.navigate(['/login']);
			}
		}
		finally {
			this.isPosting = false;
		}
	}

	private _setForm(data: any): any {

		this.formInputs = [
			new TextBox({
				type: 'text',
				key: 'name',
				label: '项目名称',
				value: data ? data.name : '',
				required: true,
				order: 0,
				groupId: 0,
				validators: Validators.required
			}),
			// new TextBox({
			// 	type: 'text',
			// 	key: 'province',
			// 	label: '省',
			// 	value: data ? data.province : '',
			// 	required: true,
			// 	order: 1,
			// 	groupId: 1,
			// 	require: true,
			// 	validators: Validators.required
			// }),
			// new TextBox({
			// 	type: 'text',
			// 	key: 'city',
			// 	label: '城市',
			// 	value: data ? data.city : '',
			// 	required: true,
			// 	order: 2,
			// 	groupId: 3,
			// 	require: true,
			// 	validators: [Validators.required]
			// }),
			new TextBox({
				type: 'text',
				key: 'faceApiUserId',
				label: 'faceApiUserId',
				value: data ? data.faceApiUserId : '',
				required: true,
				order: 4,
				groupId: 0,
				require: true,
				validators: [Validators.required]
			}),
			new TextBox({
				type: 'text',
				key: 'faceApiPass',
				label: 'faceApiPass',
				value: data ? data.faceApiPass : '',
				required: true,
				order: 2,
				groupId: 3,
				require: true,
				validators: [Validators.required]
			}),
			new TextBox({
				type: 'text',
				key: 'locationCode',
				label: 'locationCode',
				value: data ? data.locationCode : '',
				required: true,
				order: 2,
				groupId: 3,
				require: true,
				validators: [Validators.required]
			})
			// ,
			// new TextBox({
			// 	type: 'text',
			// 	key: 'district',
			// 	label: '辖区',
			// 	value: data ? data.district : '',
			// 	required: true,
			// 	order: 3,
			// 	groupId: 3,
			// 	require: true,
			// 	validators: [Validators.required]
			// 	// ,
			// 	// validators: [Validators.required, EmailValidator()]
			// })
		];
		return Helper.setGroupForm(this.formInputs);
	}
	async save(df: any) {
		this.isFormPosting = true;
		let form = <FormGroup>df.form;
		if (!form.valid) {
			return false;
		}
		let request: Project = {
			name: form.value.name,
			locationCode: form.value.locationCode,
			faceApiUserId: form.value.faceApiUserId,
			faceApiPass: form.value.faceApiPass
		};
		let action;
		if (!this.IsEdit) {
			action = this._projectService.Create(request).toPromise();
		}
		else {
			request.id = this._editProject.id;
			action = this._projectService.Update(request).toPromise();
		}
		try {
			let resp = await action;
			if (resp) {
				this._mdlSnackbarService.showToast('保存成功！', 2000);
				this._router.navigate(['/project']);
			}
		}
		catch (e) {
			this._mdlSnackbarService.showToast('操作失败，请稍后重试！', 2000);
		}
		finally {
			this.isFormPosting = false;
		}
	}
	public onLoadFormData(form) {
		console.log(form);
	}
}
