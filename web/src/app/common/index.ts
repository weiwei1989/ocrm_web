import * as _ from 'lodash';
import { ReflectiveInjector, Injectable } from '@angular/core';
import { MdlSnackbarService } from '@angular-mdl/core';

export class Helper {
    static setGroupForm(formInputs: any[]): any[] {
        formInputs = formInputs.sort((a, b) => a.order - b.order);

        let grouped = _.groupBy(formInputs, (f) => {
            return f.groupId;
        });
        // console.log(grouped);
        let retArr = [];
        for (let g in grouped) {
            if (g && grouped[g].length && grouped[g].length > 1) {
                const len = grouped[g].length;
                let tempArr = [];
                _.forEach(grouped[g], (item) => {
                    tempArr.push(item);
                });
                retArr.push(tempArr);
            }
            else {
                retArr.push(grouped[g][0])
            }
        }
        // console.log(retArr);
        return retArr;
    }
    static showErrorSnackMsg(msg: string) {

        var injector = ReflectiveInjector.resolveAndCreate([MdlSnackbarService])
        var mdkSnack = injector.get(MdlSnackbarService);
        mdkSnack.showToast(msg, 2000);
        // var this._mdlSnackbarService.showToast('操作失败，请稍后重试！', 2000);
    }
}
