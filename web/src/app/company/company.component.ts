import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Company, SortType, CompanySearchCriteria } from '../../entities/entities';
import { AuthService } from '../../services/auth-service';
// import { CompanyService } from '../../services/company-service';
import { Router, ActivatedRoute, Params, Route } from '@angular/router';

@Component({
	selector: 'app-company',
	templateUrl: './company.component.html',
	styleUrls: ['./company.component.scss']
	// ,
	// providers: [CompanyService]
})
export class CompanyComponent implements OnInit {

	constructor(
		// private _companyService: CompanyService
	) { }

	ngOnInit() {

	}

}
