import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistCmpComponent } from './vist-cmp.component';

describe('VistCmpComponent', () => {
  let component: VistCmpComponent;
  let fixture: ComponentFixture<VistCmpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistCmpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistCmpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
