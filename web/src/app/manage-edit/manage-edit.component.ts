import { ROLE_MAP } from '../../app.config';
import { ProjectService } from '../../services/project-service';
import { EmailValidator } from '../../shared/dynamicform/my-validator';
import { ManageService } from '../../services/manage-service';
import { Company, Manage, Project } from '../../shared/model/defination';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ControlBase } from 'shared/dynamicform/controlbase';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from 'services/company-service';
import { MdlSnackbarService } from '@angular-mdl/core/components';
import { SessionService } from 'services/session-service';
import { SelectBox } from 'shared/dynamicform/selectbox-control';
import { TextBox } from 'shared/dynamicform/textbox-control';
import { MobileValidator } from 'shared/dynamicform/my-validator';
import { Helper } from 'app/common';
import { FormGroup, Validators } from '@angular/forms';
import { DynamicFormComponent } from 'shared/dynamicform/dynamic-form.component';

@Component({
    selector: 'app-manage-edit',
    templateUrl: './manage-edit.component.html',
    styleUrls: ['./manage-edit.component.scss'],
    providers: [CompanyService, ManageService, ProjectService]
})
export class ManageEditComponent implements OnInit {
    public isPosting: boolean = false;
    public get IsEdit() {
        return this._route.snapshot.params['id'] !== undefined;
    }
    public get CanEditProject() {
        return this.userRole === '管理员';
    }
    public get CanEditCompany() {
        return this.userRole.toLowerCase() !== 'hr';
    }

    private _editItem: Manage;
    public isFormPosting: boolean = false;

    public locationProperties: Array<Location> = [];
    // ----------------------selected project----------------------------

    // public selectedBuilding: number;
    public selectedGender: number;
    public uploadPercent = 0;
    formInputs: ControlBase<any>[];
    formControls: any;
    public selectedRole: number;
    public roleMap: Array<{ key: string; value: number; alias: string }> = [
        { key: '管理员', value: 1, alias: '管理员' },
        { key: '项目管理员', value: 2, alias: '项目管理员' },
        { key: '公司行政／HR', value: 3, alias: '公司行政／HR' }
    ];
    public avatarPhoto: any[] = [];
    public faceIds: number[] = [];
    public identifyPhoto: any[] = [];
    public userRole = '';
    public projects: Array<Project> = [];
    public selectedProject: Project;
    public company: any;
    // @ViewChild('userForm') userForm: DynamicFormComponent;

    // public uploadedPhotoIds: number[] = [];

    constructor(
        private _route: ActivatedRoute,
        private _router: Router,
        private _companyService: CompanyService,
        private _mdlSnackbarService: MdlSnackbarService,
        private _sessionService: SessionService,
        private _manageService: ManageService,
        private _projectService: ProjectService
    ) {}

    async ngOnInit() {
        try {
            this.isPosting = true;
            this.projects = await this._projectService.All().toPromise();
            const session: Manage = await this._sessionService.getCurrentUser();
            this.userRole = session.roles[0].roleName;
            const userId = this.IsEdit ? +this._route.snapshot.params['id'] : 0;
            if (userId) {
                this._editItem = await this._manageService
                    .Get(userId)
                    .toPromise();
                const editUserRoleName = this._editItem.roles[0].roleName;
                this.selectedGender = this._editItem.gender;
                // this.selectedRole = this.roleMap.find(rm => rm.alias === editUserRoleName).value;
                this.selectedRole = this._editItem.roles.map(r => r.id)[0];
                if (this.selectedRole === 2) {
                    this.selectedProject = (this
                        ._editItem as any).managements[0].managable.id;
                } else if (this.selectedRole === 3) {
                    // company
                    const editItem = this._editItem as any;
                    this.locationProperties =
                        editItem.managements[0].managable.locations;
                    this.company = [editItem.managements[0].managable];
                }
            } else if (session.roles[0].roleName !== '管理员') {
                // 获取sessionUser的相关项信息，所属项目，如果是物业
                const sessionUser = await this._manageService
                    .Get(session.id)
                    .toPromise();
                const companyId = sessionUser.company.id;
                const company = await this._companyService
                    .Get(companyId)
                    .toPromise();
            }

            this.formControls = this._setForm(this._editItem);
        } catch (e) {
            console.log(e);
            if (e.status === 401) {
                this._router.navigate(['/login']);
            }
        } finally {
            this.isPosting = false;
        }
    }
    public uploaded(event) {
        console.log(event);
        console.log(this.avatarPhoto);
    }

    public PercentCallBack(event) {
        console.log(this.uploadPercent);
        // console.log(event);
    }
    public uploaded2(event) {
        console.log(event);
        console.log(this.identifyPhoto);
        this.faceIds = this.identifyPhoto.map(ip => ip.faceId);
        console.log(this.faceIds);
    }
    private _setForm(data: any): any {
        this.formInputs = [
            new SelectBox({
                key: 'roleId',
                label: '角色',
                options: this._getCanEditRoles(),
                value: this.selectedRole,
                order: 4,
                groupId: 4,
                require: true,
                // disabled: !this.CanEditCompany,
                validators: Validators.required
            }),
            new TextBox({
                type: 'text',
                key: 'mobile',
                label: '手机号',
                value: data ? data.mobile : '',
                required: true,
                disabled: this.IsEdit,
                order: 10,
                groupId: 5,
                require: true,
                validators: [Validators.required, MobileValidator()]
            }),
            new TextBox({
                type: 'text',
                key: 'email',
                label: 'Email',
                value: data ? data.email : '',
                required: true,
                order: 1,
                groupId: 2,
                require: true,
                validators: [Validators.required, EmailValidator()]
            }),
            new TextBox({
                type: 'text',
                key: 'userName',
                label: '姓名',
                value: data ? data.userName : '',
                required: true,
                order: 2,
                groupId: 1,
                require: true,
                validators: Validators.required
            }),
            new SelectBox({
                key: 'gender',
                label: '性别',
                options: [{ key: '女', value: '0' }, { key: '男', value: '1' }],
                value: this.selectedGender,
                order: 4,
                groupId: 3,
                require: true,
                // disabled: !this.CanEditCompany,
                validators: Validators.required
            })
        ];
        return Helper.setGroupForm(this.formInputs);
    }
    async save(df: any) {
        this.isFormPosting = true;
        const form = <FormGroup>df.form;
        if (!form.valid) {
            return false;
        }
        const request: any = {
            userName: form.value.userName,
            gender: form.value.gender,
            mobile: form.value.mobile,
            password: '88888888',
            email: form.value.email || 'user@capitaland.com',
            // companyId: form.value.companyId,
            roleIds: [form.value.roleId]
        };
        if (this.selectedRole === 2) {
            request.manageId = this.selectedProject;
            request.manageType = 'project';
        } else if (this.selectedRole === 3) {
            request.manageId = this.company[0].id;
            request.manageType = 'company';
        }
        let action;
        if (!this.IsEdit) {
            action = this._manageService.Create(request).toPromise();
        } else {
            request.id = this._editItem.id;
            // request.faceId = this._editItem.faceId;
            action = this._manageService.Update(request).toPromise();
        }
        try {
            const resp = await action;
            if (resp) {
                this._mdlSnackbarService.showToast('保存成功！', 2000);
                this._router.navigate(['/manage']);
            }
        } catch (e) {
            this._mdlSnackbarService.showToast(e.message, 2000);
        } finally {
            this.isFormPosting = false;
        }
    }

    private _getCanEditRoles() {
        const sessionRole = this.userRole.toLowerCase();
        const sessionRoleMap = this.roleMap.find(
            r => r.key.toLowerCase() === sessionRole
        );

        return this.roleMap.filter(r => r.value > sessionRoleMap.value);
    }

    public async onLoadFormData(form) {
        this.selectedRole = form.roleId;

        // if (this.selectedProject === form.projectId
        //   && this.selectedBuilding === form.buildingId
        //   && this.selectedFloor === form.floorId
        // ) return;

        // this.selectedProject = form.projectId;
        // this.selectedBuilding = form.buildingId;
        // this.selectedFloor = form.floorId;
        // this.selectedCompany = form.CompanyId;
        // console.log(`%c${this.selectedProject}--------${form.projectId}`, 'color: blue');
        // console.log(`%c${this.selectedBuilding}--------${form.buidingId}`, 'color: blue');
        // // 响应project select
        // let projectId = form.projectId;
        // if (!projectId) {
        // 	this.selectedBuilding = this.selectedFloor = this.selectedCompany = -1;
        // }
        // let building: SelectBox = <SelectBox>this.formInputs.find(f => f.key === 'buildingId')
        // building.options = this.locationConfigs[projectId];
        // // 响应building select
        // let buildingId = form.buildingId;
        // let floor: SelectBox = <SelectBox>this.formInputs.find(f => f.key === 'floorId');
        // floor.options = this.floorConfigs[buildingId];

        // // 响应floor select
        // let company: SelectBox = <SelectBox>this.formInputs.find(f => f.key === 'companyId');
        // let companySource$ = this._companyService.getCompanyiesByFloorId(form.floorId);
        // let companies = await companySource$.toPromise();
        // company.options = (companies as any).items.map(c => {
        // 	return { key: c.companyName, value: c.id };
        // })
    }
}
