import { PreviewImagesComponent } from '../shared/preview-image/preview-images.component';
import { AuthGuard } from '../services/auth-guard.service';
import { LocationSelectModule } from '../shared/location-select/location-select.module';
import { AuthInterceptor } from '../services/auth-inteceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MdlModule } from '@angular-mdl/core';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ProjectComponent } from './project/project.component';
import { CompanyComponent } from './company/company.component';
import { UserComponent } from './user/user.component';
import { LoginComponent } from './login/login.component';
import { ProjectEditComponent } from './project/project-edit/project-edit.component';
import { ProjectIndexComponent } from './project/project-index/project-index.component';
import { ControlComponent } from '../shared/dynamicform/control.component';
import { DynamicFormComponent } from '../shared/dynamicform/dynamic-form.component';
import { LocalStorageModule } from 'angular-2-local-storage';
import { SessionService } from '../services/session-service';
import { FloorComponent } from './floor/floor.component';
import { IndexComponent as FloorIndexComponent } from './floor/index/index.component';
import { EditComponent as FloorEditComponent } from './floor/edit/edit.component';
import { IndexComponent as CompanyIndexComponent } from './company/index/index.component';
import { EditComponent as CompanyEditComponent } from './company/edit/edit.component';
import { IndexComponent as UserIndexComponent } from './user/index/index.component';
import { EditComponent as UserEditComponent } from './user/edit/edit.component';
import { PhotoUploadComponent } from '../shared/photo_upload/photo-upload.component';
import { CustomDisabledDirective } from '../shared/directives/mydisabled.directive';
import { RolePipe } from '../pipes/role-pipe';
import { MdlSelectModule } from '@angular-mdl/select';
import { ChangepassComponent } from './user/changepass/changepass.component';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { PropertyModule } from 'app/property/property.module';
import { ManageComponent } from './manage/manage.component';
import { ManageEditComponent } from './manage-edit/manage-edit.component';
import { RegisterComponent } from './register/register.component';
import { CompanyPipe } from 'pipes/company-pipe';
import { EventComponentComponent } from './event-component/event-component.component';
import { MdlDatePickerModule } from '@angular-mdl/datepicker';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VistCmpComponent } from './vist-cmp/vist-cmp.component';

@NgModule({
    declarations: [
        AppComponent,
        CustomDisabledDirective,
        RolePipe,
        CompanyPipe,
        PhotoUploadComponent,
        PreviewImagesComponent,
        ControlComponent,
        DynamicFormComponent,
        ProjectComponent,
        CompanyComponent,
        UserComponent,
        LoginComponent,
        ProjectEditComponent,
        ProjectIndexComponent,
        FloorComponent,
        FloorIndexComponent,
        FloorEditComponent,
        CompanyIndexComponent,
        CompanyEditComponent,
        UserIndexComponent,
        UserEditComponent,
        ChangepassComponent,
        ManageComponent,
        ManageEditComponent,
        RegisterComponent,
        EventComponentComponent,
        DashboardComponent,
        VistCmpComponent
    ],
    imports: [
        MdlDatePickerModule,
        MdlSelectModule,
        LocalStorageModule.withConfig({
            prefix: 'oc',
            storageType: 'localStorage'
        }),
        MdlModule,
        NgxDatatableModule,
        BrowserModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        PropertyModule,
        HttpModule,
        AppRoutingModule,
        LocationSelectModule
    ],
    providers: [
        SessionService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        AuthGuard
    ],
    entryComponents: [PreviewImagesComponent],
    bootstrap: [AppComponent]
})
export class AppModule {}
