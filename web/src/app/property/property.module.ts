import { UnitService } from '../../services/unit-service';
import { FloorService } from '../../services/floor-service';
import { BuildingService } from '../../services/building-service';
import { HoverActionDirective } from './hover-action.directive';
import { PropertyRoutingModule } from './property.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PropertyComponent } from './property.component';
import { MdlModule } from '@angular-mdl/core';
import { FormsModule } from "@angular/forms";

@NgModule({
  imports: [
    CommonModule,
    PropertyRoutingModule,
    MdlModule,
    FormsModule
  ],
  declarations: [PropertyComponent, HoverActionDirective],
  providers: [BuildingService, FloorService, UnitService],
  exports: [PropertyComponent]
})
export class PropertyModule { }
