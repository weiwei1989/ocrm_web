import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PropertyComponent } from './property.component';

// import { AuthGuard } from '../auth-guard.service';

const propertyRoutes: Routes = [
    {
        path: 'property',
        component: PropertyComponent,
        // canActivate: [AuthGuard],
        children: [
            {
                path: '',
                // canActivateChild: [AuthGuard],
                children: [
                    { path: '', component: PropertyComponent }
                    // ,
                    // { path: 'heroes', component: ManageHeroesComponent },
                    // { path: '', component: AdminDashboardComponent }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(propertyRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class PropertyRoutingModule { }


/*
Copyright 2017 Google Inc. All Rights Reserved.
Use of this source code is governed by an MIT-style license that
can be found in the LICENSE file at http://angular.io/license
*/