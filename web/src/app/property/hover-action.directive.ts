import { Directive, ElementRef, Input, HostListener } from '@angular/core';
import * as $ from 'jquery';
@Directive({ selector: '[hoverAction]' })
export class HoverActionDirective {
    constructor(private _el: ElementRef) {
    }

    @HostListener('mouseenter')
    onMouseEnter() {
        $(this._el.nativeElement).find('.icon-wrap').css('visibility', 'visible');
    }

    @HostListener('mouseleave')
    onMouseLeave() {
        $(this._el.nativeElement).find('.icon-wrap').css('visibility', 'hidden');
    }
}