import { environment } from 'environments/environment';

export interface Menu {
  text: string;
  path: string;
  icon: string;
  allowRoles: Array<string>;
}
export const USER_ROLE = ['管理员', '项目管理员', '公司行政／HR'];
export enum ROLE_MAP {
  SUPER = '管理员',
  PROPERTY = '2',
  HR = '3'
}
export const slideMenus: Menu[] = [
  {
    text: '项目管理',
    path: '/project',
    icon: 'business',
    allowRoles: ['管理员']
  },
  {
    text: '楼层单元管理',
    path: '/property',
    icon: 'account_balance',
    allowRoles: ['管理员', '项目管理员']
  },
  {
    text: '管理员列表',
    path: '/manage',
    icon: 'assignment',
    allowRoles: ['管理员', '项目管理员']
  },
  {
    text: '公司管理',
    path: '/company',
    icon: 'grade',
    allowRoles: ['管理员', '项目管理员']
  },
  {
    text: '人员管理',
    path: '/user',
    icon: 'assignment_ind',
    allowRoles: ['管理员', '项目管理员', '公司行政／HR']
  },
  {
    text: '门禁出入记录',
    path: '/events',
    icon: 'assignment_ind',
    allowRoles: ['管理员', '项目管理员']
  },
  {
    text: '访客申请',
    path: '/visit',
    icon: 'assignment_ind',
    allowRoles: ['管理员', '项目管理员']
  }
];

export const TOKEN_PREFIX = 'OC-TOKEN';
export const TOKEN_STORAGE_KEY = 'access_token';
export const USER_STORAGE_KEY = 'auth_user';
export const DOMAIN = environment.endPoint;
export const API_VERSION = 'v1';
