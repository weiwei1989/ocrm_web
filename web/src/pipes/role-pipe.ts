import { Role } from '../shared/model/defination';
import { Pipe, PipeTransform } from '@angular/core';
import { Menu } from '../app.config';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 |  exponentialStrength:10}}
 *   formats to: 1024
*/

@Pipe({ name: 'role' })
export class RolePipe implements PipeTransform {
    public roleMap: Array<any> = ['管理员', '项目管理员', '公司行政／HR'];

    transform(menu: Menu[], userRole: Role): Menu[] {
        if (!userRole) {
            return [];
        }
        console.log(userRole);
        return menu.filter(
            // m => m.allowRoles.indexOf(userRole.roleName.toLowerCase()) !== -1
            m => m.allowRoles.indexOf(userRole.roleName) !== -1
        );
    }
}
