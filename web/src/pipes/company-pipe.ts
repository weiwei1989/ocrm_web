import { Pipe, PipeTransform } from '@angular/core';
/*
 * Raise the value exponentially
 * Takes an exponent argument that defaults to 1.
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 |  exponentialStrength:10}}
 *   formats to: 1024
*/

@Pipe({ name: 'filterCompany' })
export class CompanyPipe implements PipeTransform {
    transform(inputs: any[], args?: string): any[] {
        if (inputs && inputs.length) {
            return inputs.filter(c => c.key.indexOf(args) !== -1);
        }
    }
}
