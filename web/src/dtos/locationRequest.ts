export interface LocationRequest {
	id?: number;
	projectId: number;
	buildingNumber: string;
	// floorIndex: number;
	// unitCode: string;
}
