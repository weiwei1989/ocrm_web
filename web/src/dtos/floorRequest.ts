export interface FloorRequest {
	id?: number;
	index: number;
	name: string;
	buildingId: number;
}