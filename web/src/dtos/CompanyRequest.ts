export interface CompanyLocation {
	companyFloorId: number;
	unitCode: string;
	companyId: number;
}

export interface CompanyRequest {
	id?: number;
	name: string;
	// companyLocations: CompanyLocation[];
	phone: string;
	locationIds: number[];
	// unitCode: string;
}
