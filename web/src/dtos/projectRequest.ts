export interface ProjectRequest {
	id?: number;
	projectName: string;
	district: string;
	city: string;
	province: string;
	internalLocationId: string;
	// locationIds: number[];
}
